<<<<<<< HEAD

# Jr Data Scientist - Evaluation -1 Assignment Documentation

This README.md file contains the Documentation of the Jr Data Scientist - Evaluation -1 Assignment for NextGrowthLabs.




# Documentation

# **Part 1**

# **Question 1**

Write a regex to extract all the numbers with orange color background from the below text in italics.


{"orders":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},{"id":8},{"id":9},{"id":10},{"id":11},{"id":648},{"id":649},

{"id":650},{"id":651},{"id":652},{"id":653}],"errors":[{"code":3,"message":"[PHP Warning #2] 

count(): Parameter must be an array or an object that implements Countable (153)"}]}

__*Solution*__

#The regex to extract the said numbers is as follows:

```bash
import re

match = re.findall(r':[0-9]+','{"orders":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},
{"id":8},{"id":9},{"id":10},{"id":11},{"id":648},{"id":649},{"id":650},{"id":651},{"id":652},{"id":653}],
"errors":[{"code":3,"message":"[PHP Warning #2] count(): Parameter must be an array or an object that implements Countable (153)"}]}')

for i in range(0,len(match)):
    print(match[i][1:])
```
# **Question 2**

Identify the reviews where the semantics of review text does not match rating. 
Deploy it using - Flask/Streamlit etc and share the live link. 

__*Solution*__

The streamlit live link for deployed app is: <https://share.streamlit.io/namratharaj7/chrome_reviews/main/Chrome_review_deployed.py>

#Identification of reviews with good text reviews and negative Star rating and deploying the same usind Streamlit.

#The code for this problem is in the Chrome_review_deployed.py

#The libraries and packages required for this problem include

- pandas (for reading file and performing operations)

- SentimentIntensityAnalyzer, vader_lexicon from NLTK (for sentiment analysis)

- TextBlob from TextBlob (for sentiment analysis)

- Streamlit (for web app deployment)



### The steps involved in solving:
    
#### 1. importing necessary libraries & packages listed above.
    
    ```bash
    import pandas as pd

    import nltk
    from nltk.sentiment import SentimentIntensityAnalyzer
    nltk.download('vader_lexicon')

    from textblob import TextBlob

    import streamlit as st
    ```
#### 2. reading the csv file of the chrome_review dataset
```bash
  df =  pd.read_csv('chrome_reviews.csv')
```
    
#### 3. analysing the data by calling .info and and checking for null values 
```bash
df.info()
```
#read the 'Text' column as string
```bash
df['Text'] = df['Text'].astype(str)
```

#### 4. defining the function to return the sentiment polarity score of the text review using textblob library
```bash
  def find_pol(text):
      return TextBlob(text).sentiment.polarity
```
##applying the above function on the 'Text' column & saving the sentiment polarity value in a new column named 'S_polarity'
```bash
df['S_polarity'] = df['Text'].apply(find_pol)
```
**Note**:

*The value of S_polarity lies between -1 & 1.*

*S_polarity value <0 indicates a Negative sentiment*,

*S_polarity value >0 indicates a Positive sentiment*,

*S_polarity value =0 indicates a Neutral sentiment*

ex:

S_polarity value = -1 indicates  Most Negative sentiment(100% Negative)
&
S_polarity value = 1 indicates Most Positive sentiment(100% Positive)

#### 5. defining another sentiment function using SentimentIntensityAnalyzer from nltk library to better achieve the desired result
```bash
def sentiment_analyse(text):
    score = SentimentIntensityAnalyzer().polarity_scores(text)
    if score['neg'] > score['pos']:
            return "Negative"
    elif score['neg'] < score['pos']:
            return "Positive"
    else:
            return "Neutral"
```
#applying the above function on the 'Text' column & saving the sentiment in a new column named 'Sentiment'
```bash
df['Sentiment']=df['Text'].apply(sentiment_analyse)
```
#### 6. Obtaining the data of reviews with good text review & bad rating
> Let us consider that the star ratings of (1,2) are bad, 3 is neutral and (4,5) is good

> Since we want to find the reviews with bad star rating(1,2) & good/positive sentiment

> let us use the following 3 parameters to obtained the desired result:

a. To filter bad ratings: **(df['Star'] <3)**

b. To filter positive sentiment from *find_pol* function: **(df['S_polarity'] >= 0.6)**

c. To filter positive sentiment from *sentiment_analyse* function: **(df['Sentiment'] == 'Positive')**
   
   Here 0.6 indicates 60% & above level of positiveness in the review

#combining all the paramters we can get the dataframe of the discrepency reviews as:

```bash
df_discrepency_reviews = df[(df['S_polarity'] >= 0.6) & (df['Star'] <3) & (df['Sentiment']=='Positive')]
```
#### 7. Code for Deployment of the model in Streamlit

* #### defining the title of the web app
```bash
st.title("Chrome Reviews having Positive Text Reviews with Negative Star Ratings")
```
* #### Creating to CSV file upload option
```bash
uploaded_file = st.file_uploader("Choose a CSV file for checking review-rating discrepancy",type=["csv"])
```
* #### Performing the steps (4 to 6) for the uploaded test csv file
```bash
if uploaded_file is not None:
    df_test = pd.read_csv(uploaded_file)
    df_test['Text'] = df_test['Text'].astype(str)
    df_test['Sentiment']=df_test['Text'].apply(sentiment_analyse)
    df_test['S_polarity'] = df_test['Text'].apply(find_pol)
    df_test_discrepency_reviews = df_test[(df_test['S_polarity'] > 0.65) & (df_test['Star'] <3) 
                                    & (df_test['Sentiment']=='Positive')]
```
* #### Display of reviews having good text reviews & negative star ratings on web app using streamlit
```bash
st.write("The list of reviews data having text reviews and star ratings mismatch:")
st.write(df_test_discrepency_reviews)
```

The streamlit live link for deployed app is: <https://share.streamlit.io/namratharaj7/chrome_reviews/main/Chrome_review_deployed.py>

**Question 2**
Ranking Data - Understanding the co-relation between keyword rankings with description or any other attribute. 

__*Solution*__
* Presence of keyword in the initial or first line has a better rank than others with later presence of keyword in the description.
* The rank has a direct corelation with the 'app id'. The keyword being present in the 'app id' have a better rank than otherwise.
* Short description has a sharp corelation with the rank. The rank is better when the short description is framed catchy using the keywords & supportive adjectives.
* The first line in the long description as what may be called as a Header is a key contributor to the rank.

## **Part 2**

#Check if the sentence is Grammatically correct: Please use any pre-trained model or use text from
#open datasets. Once done, please evaluate the English Grammar in the text column of the given dataset.


__*Solution*__

There are many pretrained models available(GingerIt, happytransformer, python language-tool, bing,etc ) for python to check the english grammar. 
However based on the performance and output precision, **language-tool** is used to solve this problem.

Prerequisites to run code in python:
* LanguageTool
* Java installation


#import the language_tool_python & create instance as *tool*
```
import language_tool_python
tool = language_tool_python.LanguageTool('en-US')
```
#import pandas and read the file
```
import pandas as pd

df = pd.read_csv('review_data.csv')
```
#(Optional Step)Since we are working with only text column, let us drop other columns for better readability of dataframe
```
df.drop(labels=['star','app_id','reviewDate'],axis=1,inplace=True)
```
#define a function,*grammar* to check grammatical correctness
```
def grammar(text):
    matches = tool.check(text)
    count = len(matches)
    if count == 0:
        return 'Correct'
    else:
        return 'Incorrect'
```
#apply the above function on the 'text' column & save the output in a new column named 'Grammar'
```
df['Grammar'] = df['text'].apply(lambda text:grammar(text))
```
Now the result of grammatical correctness of the text either(correct/incorrect) is displayed in the corresponding Grammar column

### Bonus/Optional - indicate the grammatical accuracy of sentences in percentage or on number scale (1-10), that would be an added plus - but is not essential. 

An easy way to get the accuracy is by finding the no. of mistakes & total no. of words in each text & applying the below formula on the 'text' column to get the grammar accuracy:
Grammar Accuracy% = ((Total no. of Words - No. of mistakes)*100)/ Total no. of Words

#Use apply function on the text column as below to store the no. of mistakes in new column called 'Mistake_count'
```
df['Mistake_count'] = df['text'].apply(lambda text:len(tool.check(text)))
```
#Similarly use apply function again on the text column as below to store the total no. of words in new column called 'Word_count'
```
df['Word_count'] = df['text'].apply(lambda word:len(word.split()))
```
Now that the mistake count & word count are know, we can get the Grammar accuracy % as in the last step as:

#Create a new column to store the Grammar_Accuracy in percentage as 'Grammar_Accuracy%' using the below formula
```
df['Grammar_Accuracy%'] = ((df['Word_count']-df['Mistake_count'])*100)/(df['Word_count'])
```
Now this stores the grammatical accuracy of sentences in percentage.


## Bonus Questions:

## Question 1

Write about any difficult problem that you solved. (According to us difficult - is something which 90% of people would have only 10% probability in getting a similarly good solution). 

As part of my academic research project recently during my masters degree, I worked on modelling the control aspects of automated driving car.The model is developed in MATLAB platform.
The problem that I have attempted to solve is the control of the steering angle of the autonomous vehicle in the highway traffic while performing a Lane change manevuer.
I had developed a Predictive Control algorithm using [Model Predictive Controller](https://ch.mathworks.com/help/mpc/ref/mpc.html?searchHighlight=MPC&s_tid=srchtitle_MPC_1) along with A-star path planner to design a collision-free reference path for the automated car to follow.
The solution comes with building many submodels monitored to track the dynamic aspects of the vehicle in particular the speed & the yaw angle.
The interesting part of doing this project for me was developing a 3D model of a highway traffic scenario making use of the [Driving Scenario Designer](https://ch.mathworks.com/help/driving/ref/drivingscenariodesigner-app.html).
This project was possible with the guidance from the MathWorks team themself while I attended the MatLab Expo 2021.

In addition to designing the control aspects, I have also predicted the lanes of the surrounding vehicles using the famous [NGSIM dataset](https://datahub.transportation.gov/Automobiles/Next-Generation-Simulation-NGSIM-Vehicle-Trajector/8ect-6jqj),
where I have cleaned the data having 11 million records of data, trained them in chunks in various supervised ML algorithms like SVM, KNN, Regression, Naive Bayes etc & also in DL's neural networks 
making use of the ML & DL apps([ClassificationLearner](https://ch.mathworks.com/help/stats/classificationlearner-app.html?searchHighlight=classification%20learner&s_tid=srchtitle_classification%2520learner_1), [NeuralNetFitting](https://ch.mathworks.com/help/deeplearning/gs/fit-data-with-a-neural-network.html)) available in MATLAB to obtain the prediction accuracy of 99.8%

## Question 2

Formally, a vector space V' is a subspace of a vector space V if
V' is a vector space
every element of V′ is also an element of V.
Note that ordered pairs of real numbers (a,b) a,b∈R form a vector space V. Which of the following is a subspace of V?
- The set of pairs (a, a + 1) for all real a
- The set of pairs (a, b) for all real a ≥ b
- The set of pairs (a, 2a) for all real a
- The set of pairs (a, b) for all non-negative real a,b

__*Solution*__

#### The set of pairs (a, 2a) for all real a

#### The answer is as above as it satisfies the conditions of zero vector (0,0), addition rule and scalar multiplication criterion. 
#### So, the answer is set of pairs (a,2a) for all real a, form the subspace of vector space V.
=======


# Evaluation Assessment

Evaluation assessment of Next Labs interview process for Jr. Data scientist

