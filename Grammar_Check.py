#!/usr/bin/env python
# coding: utf-8

# In[2]:


#import the language_tool_python & create instance as *tool* for the english language
import language_tool_python
tool = language_tool_python.LanguageTool('en-US')


# In[39]:


#import pandas and read the file
import pandas as pd
df = pd.read_csv('review_data.csv')


# In[41]:


#Let us keep only the text column & drop others for better readability of the dataframe
df.drop(labels=['star','app_id','reviewDate'],axis=1,inplace=True)


# In[12]:


#define a function,*grammar* to check grammatical correctness
def grammar(text):
    matches = tool.check(text)
    count = len(matches)
    if count == 0:
        return 'Correct'
    else:
        return 'Incorrect'


# In[47]:


#apply the above function on the 'text' column & save the output in a new column named 'Grammar'
df['Grammar'] = df['text'].apply(lambda text:grammar(text))


# Now the result of grammatical correctness of the text either(correct/incorrect) is displayed in the corresponding Grammar column

# In[ ]:





# In[50]:


#Bonus/Optional - indicate the grammatical accuracy of sentences in percentage or on number scale (1-10), that would be an added plus - but is not essential. 


# An easy way to get the accuracy is by finding the no. of mistakes & total no. of words in each text & applying the below formula on the 'text' column to get the grammar accuracy:
# 
# Grammar Accuracy% = ((Total no. of Words - No. of mistakes)*100)/ Total no. of Words

# In[51]:


#Use apply function on the text column as below to store the no. of mistakes in new column called 'Mistake_count'
df['Mistake_count'] = df['text'].apply(lambda text:len(tool.check(text)))

#Similarly use apply function again on the text column as below to store the total no. of words in new column called 'Word_count'
df['Word_count'] = df['text'].apply(lambda word:len(word.split()))


# Now that the mistake count & word count are know, we can get the Grammar accuracy % as in the last step as:

# In[53]:


#Create a new column to store the Grammar_Accuracy in percentage as 'Grammar_Accuracy%' using the below formula
df['Grammar_Accuracy%'] = ((df['Word_count']-df['Mistake_count'])*100)/(df['Word_count'])

